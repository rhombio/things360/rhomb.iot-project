#pragma once

#include <unity.h>

void test_OPMODE_DEFAULT_IS_SET(void) {
  TEST_ASSERT_EQUAL(OPMODE_DEFAULT, cfg::get.opmode);
}

void runOpmode() { RUN_TEST(test_OPMODE_DEFAULT_IS_SET); }
