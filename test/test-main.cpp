#include <Arduino.h>
#include <unity.h>

#include "rhomb.iot.h"
#include "test-opmode.h"

void setup() {
  delay(2000);

  core::setup();

  UNITY_BEGIN();
  runOpmode();
  UNITY_END();
}

void loop() {}
