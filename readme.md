# Rhomb.IoT Project for VS Code

This is a [VS Code](https://code.visualstudio.com) project pre-configured to work with [Rhomb.IoT](https://gitlab.com/rhombio/things360/rhomb.iot) as defacto development environment.

The `.vscode` folder contains local settings and extensions recommendations to follow the project code style and other guidance that help you to focus on code and not on configure the editor.

## Install & Usage

Read the [Getting Started](https://rhiot.rhomb.io/users/)

## Development

This project is open source, you are welcome to send issues and pull requests.
